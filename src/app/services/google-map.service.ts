import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GoogleMapService {

  constructor(private http: HttpClient) { }

  getQuery( query:string ) {

  	const url = `https://maps.googleapis.com/maps/api/geocode/${ query }`;

  	return this.http.get(url);
  }

  //palce must be URL encode
  getPlace(latitude:string ,longitude:string) {
  	return this.getQuery(`json?latlng=${latitude},${longitude}&key=AIzaSyCAMPdt2tG5j10eSGmIXvcXKr08JTcZYcU`);
  }
}
