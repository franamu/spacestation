import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class CoordinatesService {

  constructor(private http: HttpClient) { 
  	console.log('servicio listo');
  }

  getNewCoordenate() {
  	const url = 'http://api.open-notify.org/iss-now.json';
  	return this.http.get(url)
  		.pipe( map(response => response['iss_position']));
  }

}