import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ImagesService {

  constructor(private http: HttpClient) { }

  getQuery( query:string ) {

  	const url = `https://pixabay.com/api/${ query }`;

  	return this.http.get(url);
  }

  //palce must be URL encode
  getImages(place:string) {
  	return this.getQuery(`?key=1247356-c9a05212344d205ec7d6c4b14&q=${ place }&image_type=photo`)
  		.pipe( map(response => response['hits']));

  }

}
