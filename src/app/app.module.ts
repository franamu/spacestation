import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { SearchComponent } from './components/search/search.component';
import { LoadingComponent } from './components/loading/loading.component';
import { TarjetasComponent } from './components/tarjetas/tarjetas.component';
import { ResponseComponent } from './components/response/response.component';

//services
import { CoordinatesService } from './services/coordinates.service';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    SearchComponent,
    ResponseComponent,
    LoadingComponent,
    TarjetasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
  CoordinatesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
