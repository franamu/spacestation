import { Component, OnInit } from '@angular/core';
import { CoordinatesService } from '../../services/coordinates.service';
import { ImagesService } from '../../services/images.service';
import { GoogleMapService } from '../../services/google-map.service';
import { ResponseComponent } from '../response/response.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

    loading:boolean;
    latitude:string;
    longitude:string;
    resultsCitys:any [] = [];
    country:string = '';
    city:string = '';
    countryAndCity = '';
    imagesCitys:any [] = [];
    maxDegrees:number = 5;
    maxMinutes:number = 4;
    originalLatitude:string;
    originalLongitude:string;
    maxLatDegree:number;
    changeLoop = false;
    countryAndCity2:any [] = [];

    constructor(private coordenada: CoordinatesService, 
    private images:ImagesService, private palces:GoogleMapService) { 
      this.loading = true;
      this.getInfo();
    }

    ngOnInit() {

    	setTimeout(()=> {
    		var button = document.getElementById('allContentButton'),
    		divs = document.getElementsByClassName('allContentB');

            if(button) {
                button.addEventListener('mouseover', function() {
                  divs['0'].classList.add('animated');
                  divs['1'].classList.add('animated');
                });
            }

            if(button) {
                button.addEventListener('mouseout', function() {
                  divs['0'].classList.remove('animated');
                  divs['1'].classList.remove('animated');
                });
            }           
    	}, 1);
    }

    getInfo() {
        this.loading = true;
        this.coordenada.getNewCoordenate()
          .subscribe((response: any) => {
            this.originalLatitude = response.latitude;
            this.originalLongitude = response.longitude;
            this.getNewCoordenates(this.originalLatitude, this.originalLongitude, true);
          });
    }

    /*
    * This function send the corrdenates to another function to get the imagens.
    * First send the original latitude and longitude but then change the values to
    * find places around 600kmg2 to the current position of the space station.
    * We will have a while to get 15 places pictures or until they are more than 600km2 area.
    */
    getNewCoordenates(latitude:string, longitude:string , firstLoop:boolean) {

        /* Latitude: each degree is (1°) = 111km
        * 600km -> 5.4°
        * 600km2 is a square so his area is a^2 
        * So the area that we want to search is 5.4^2 of current position
        * Example: -31.4385764,-64.2187916
        * -31° + 5.4° to N , S , E , O because is a square
        * We should have a special operation for minutes because of the decimal (4) in the degree number.
        * But because of time we coudn't do that operation so only works with degrees.
        */

        //the latitud and longitude degrees should dont be more than (original latitude/longitude + maxDegrees).
        this.maxLatDegree = (parseInt(this.originalLatitude) + this.maxDegrees);
        this.latitude = latitude;
        this.longitude = longitude;
        //value of original latitude
        let getLatitudeValues = this.latitude.split('.');
        let getLatitudeDegree =  (Number)(getLatitudeValues[0]);

        if(!firstLoop) {
            /* get new latitude degree */
            if(this.changeLoop) {
                getLatitudeDegree = (getLatitudeDegree + 1);
                let tempLatitude = (String)(getLatitudeDegree);
                this.latitude = tempLatitude + '.' + getLatitudeValues[1];
                this.changeLoop = false;
            } else {
                //value of original longitude
                let getLongitudeValues = this.longitude.split('.');
                let getLongitudeDegree =  (Number)(getLongitudeValues[0]);

                /* get new longitude degree */
                getLongitudeDegree = (getLongitudeDegree + 1);
                let tempLongitude = (String)(getLongitudeDegree);
                this.longitude = tempLongitude + '.' + getLongitudeValues[1];
                this.changeLoop = true;
            }

        }
        // verify area completed
        if(getLatitudeDegree < this.maxLatDegree ) {
            this.getPlace(this.latitude, this.longitude);
        } else {
            //if the area is completed we asked if we have images
            //if not, message of fail search will appears
            if(this.imagesCitys.length == 0) {
                this.imagesCitys = [];
                if(this.countryAndCity2.length > 6) {
                    this.countryAndCity2 = [];
                }
                this.countryAndCity = '';
                this.loading = false;
            }
            console.log('All palces found');
            console.log(this.countryAndCity2);
        }

    }

    getPlace(latitude:string ,longitude:string) {
        this.palces.getPlace(latitude, longitude)
        .subscribe((response: any) => {    
            console.log(response);          
            if(response.status === 'ZERO_RESULTS') {
                this.getNewCoordenates(latitude, longitude, false);

            } else {

             for (var i = 0; i < response.results.length; ++i) {
                 
                 let x = i;
                 x = x + 2;
                 if(x == response.results.length) {
                     this.countryAndCity = response.results[i].formatted_address; 
                 }
             }

            }   

            if(this.countryAndCity !== '') {
                this.countryAndCity2.push(this.countryAndCity); 
                this.countryAndCity = this.countryAndCity.replace(' ', '+');
                this.countryAndCity = this.countryAndCity.replace(',', '+');
                this.getImagen(this.countryAndCity, latitude, longitude);
            } else {
                this.countryAndCity2[0] = '';
                console.log('No se encontró lugar');
            }
        });
    }

    getImagen(country:string, latitude:string, longitude:string) {
        this.images.getImages(country)
          .subscribe((response: any) => {
              console.log(response);
              for (var i = 0; i < response.length; ++i) {
                  this.imagesCitys.push(response[i]);
              }

            if(this.imagesCitys.length < 15) {
                this.getNewCoordenates(latitude, longitude, false);
            }
            this.loading = false;
          });
    }
}
